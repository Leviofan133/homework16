﻿
#include <iostream>
#include <string>

//Создаём класс Игрок с приватными Именем и Очками, обеспечиваем к ним доступ
class Player 
{
private:
    std::string Name;
    int Points;
public:
    Player() : Name(""), Points(0) {}

    void SetPlayerData(std::string PlayerName, int PlayerPoints) 
    {
        Name = PlayerName;
        Points = PlayerPoints;
    }

    std::string GetName() 
    {
        return Name;
    }

    int GetPoints() 
    {
        return Points;
    }
};

int main() 
{
    // Выделяем память под конкретный размер массива и заполняем его

    int NumberOfPlayers;
    std::cout << "Enter the number of players: ";
    std::cin >> NumberOfPlayers;

    Player* PlayersMassive = new Player[NumberOfPlayers];

    for (int i = 0; i < NumberOfPlayers; i++)
    {
        std::string PlayerName;
        int PlayerPoints;
        std::cout << "Enter player " << i + 1 << " name: ";
        std::cin >> PlayerName;
        std::cout << "Enter player " << i + 1 << " points: ";
        std::cin >> PlayerPoints;
        PlayersMassive[i].SetPlayerData(PlayerName, PlayerPoints);
    }

  // Сортировка методом вставки 

    for (int i = 1; i < NumberOfPlayers; i++) 
    {
        Player Key = PlayersMassive[i];
        int j = i - 1;
        while (j >= 0 && PlayersMassive[j].GetPoints() < Key.GetPoints()) {
            PlayersMassive[j + 1] = PlayersMassive[j];
            j = j - 1;
        }
        PlayersMassive[j + 1] = Key;
    }

    // Вывод отсортированного массива
    std::cout << "Players and their points:\n";
    for (int i = 0; i < NumberOfPlayers; i++) 
    {
        std::cout << PlayersMassive[i].GetName() << ": " << PlayersMassive[i].GetPoints() << std::endl;
    }

    delete[] PlayersMassive;

    return 0;
}
